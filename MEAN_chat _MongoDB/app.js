var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/node-angular');

var app = express();
var PORT = 3000;

var index = require('./routes/index');


app.set('views', path.join(__dirname,"views"));
app.set('view engine', 'hbs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use('/', index);

app.listen(PORT, function(){
	console.log("Listening on port "+PORT);
});