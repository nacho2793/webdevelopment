var express = require('express');
var app = express();
var path = require('path');
var port = 3000;
var index = require('./routes/index');
var bodyParser = require('body-parser');


//Methods
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended:true
}));
//Directions
app.use('/', index);

app.listen(port, function(){
	console.log('Server listening in port ' + port);
});