var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
	first_name: {type:String, required:true},
	last_name: String,
	age: Number,
	genre: String,
	register_date:{type:Date, default:Date.now}
});

module.exports = mongoose.model('User', userSchema);