var express = require('express');
var router = express.Router();
var Persona = require('./../models/users');

var mongoose = require('mongoose');
var mDB = 'mongodb://localhost:27017/users';
mongoose.connect(mDB);

router.get('/message', function(req, res, next){
	res.json({message:"Hello World!"});
});

router.get('/show', function(req, res, next){
	mongoose.model('User').find(function(err, user){
		res.send(user);
	});
});

router.get('/show2', function(req, res, next){
	Persona.find().exec(function(err, users){
		if(err){
			res.send('Cant read DB.');
		}else{
			res.json(users);
		}
	})
});


router.post('/register', function(req, res){
	var newUser = new Persona();
    
    newUser.first_name = req.body.first_name;
    newUser.last_name = req.body.last_name;
    newUser.age = req.body.age;
    newUser.genre = req.body.genre;
    

    newUser.save(function(err,user) {
        if (err)
           res.send("ERROR: Cant save the user");
        else 
        	res.send(user);
           console.log('User added');
    });
});

module.exports = router;