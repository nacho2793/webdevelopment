var express = require('express');
var app = express();
var path = require('path');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var printJSON = require('./routes/printJSON');
var mongo = require('./routes/db');

var port = 3000;

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use('/', index);
app.use('/db', mongo);
app.use('/JSON', printJSON);

app.listen(port, function(){
	console.log('Server listening in port ' + port);
});