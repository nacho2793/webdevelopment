var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var movieSchema = new Schema({
	name: String,
	versionKey: false
});

var Movie = mongoose.model('movies', movieSchema);
module.exports = Movie;
