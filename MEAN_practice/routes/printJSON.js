var express = require('express');
var router = express.Router();

var Nacho = {
	"Nombre":"Nacho", 
	"Apellido":"Sainz",
	"Edad":23
}

router.get('/', function(req, res, next){
	res.json(Nacho);
});

module.exports = router;