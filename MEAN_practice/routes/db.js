var express = require('express');
var router = express.Router();
var movies = require('./../models/movies');

var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/movies');
var db = mongoose.connection;



router.get('/movies', function(req, res, next){
	mongoose.model('movies').find(function(err, movies){
		res.send(movies);
	});
});

router.post('/addMovie', function(request, response){
    var newMovie = new movies({
        name: request.body.name
    });

    newMovie.save(function(err) {
        if (err)
           throw err;
        else 
           console.log('Movie added');
    });
});

module.exports = router;